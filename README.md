# IOTA VHDL PoW (Pearl Diver)

IOTA’s PoW needs a lot of calculation power - about 90s in average on a Raspberry Pi.

The  PiDiver runs at ~16MHash/s and solves the PoW problem in an average time of about 300ms.

Main project website:
http://microengineer.eu/2018/04/25/iota-pearl-diver-fpga/

IOTA Discord: pmaxuw#8292

# License

This project is licensed under the MIT-License (https://opensource.org/licenses/MIT)
