################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../blue-app/iota/addresses.c \
../blue-app/iota/bundle.c \
../blue-app/iota/conversion.c \
../blue-app/iota/kerl.c \
../blue-app/iota/seed.c \
../blue-app/iota/signing.c 

OBJS += \
./blue-app/iota/addresses.o \
./blue-app/iota/bundle.o \
./blue-app/iota/conversion.o \
./blue-app/iota/kerl.o \
./blue-app/iota/seed.o \
./blue-app/iota/signing.o 

C_DEPS += \
./blue-app/iota/addresses.d \
./blue-app/iota/bundle.d \
./blue-app/iota/conversion.d \
./blue-app/iota/kerl.d \
./blue-app/iota/seed.d \
./blue-app/iota/signing.d 


# Each subdirectory must supply rules for building sources it contributes
blue-app/iota/%.o: ../blue-app/iota/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DUSE_FULL_ASSERT -DOS_USE_SEMIHOSTING -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -DSTM32F302xC -DSTM32F30X -DPIDIVER1_2 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f3-stdperiph" -I"/home/thomas/git/usbdiver_stm32/STM32_USB-FS-Device_Driver/inc" -I"/home/thomas/git/usbdiver_stm32/vcp/inc" -I"/home/thomas/git/usbdiver_stm32/keccak" -I"/home/thomas/git/usbdiver_stm32" -I"/home/thomas/git/usbdiver_stm32/blue-app" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


