################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../STM32_USB-FS-Device_Driver/src/usb_core.c \
../STM32_USB-FS-Device_Driver/src/usb_init.c \
../STM32_USB-FS-Device_Driver/src/usb_int.c \
../STM32_USB-FS-Device_Driver/src/usb_mem.c \
../STM32_USB-FS-Device_Driver/src/usb_regs.c \
../STM32_USB-FS-Device_Driver/src/usb_sil.c 

OBJS += \
./STM32_USB-FS-Device_Driver/src/usb_core.o \
./STM32_USB-FS-Device_Driver/src/usb_init.o \
./STM32_USB-FS-Device_Driver/src/usb_int.o \
./STM32_USB-FS-Device_Driver/src/usb_mem.o \
./STM32_USB-FS-Device_Driver/src/usb_regs.o \
./STM32_USB-FS-Device_Driver/src/usb_sil.o 

C_DEPS += \
./STM32_USB-FS-Device_Driver/src/usb_core.d \
./STM32_USB-FS-Device_Driver/src/usb_init.d \
./STM32_USB-FS-Device_Driver/src/usb_int.d \
./STM32_USB-FS-Device_Driver/src/usb_mem.d \
./STM32_USB-FS-Device_Driver/src/usb_regs.d \
./STM32_USB-FS-Device_Driver/src/usb_sil.d 


# Each subdirectory must supply rules for building sources it contributes
STM32_USB-FS-Device_Driver/src/%.o: ../STM32_USB-FS-Device_Driver/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DUSE_FULL_ASSERT -DOS_USE_SEMIHOSTING -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -DSTM32F302xC -DSTM32F30X -DPIDIVER1_2 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f3-stdperiph" -I"/home/thomas/git/usbdiver_stm32/STM32_USB-FS-Device_Driver/inc" -I"/home/thomas/git/usbdiver_stm32/vcp/inc" -I"/home/thomas/git/usbdiver_stm32/submodule/stm32f3-serial" -I"/home/thomas/git/usbdiver_stm32/submodule/DFRobot_SIM808" -I"/home/thomas/git/usbdiver_stm32/submodule/iota-c-light-wallet/src/iota" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


