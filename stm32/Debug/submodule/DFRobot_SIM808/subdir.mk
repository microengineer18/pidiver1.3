################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../submodule/DFRobot_SIM808/DFRobot_sim808.cpp \
../submodule/DFRobot_SIM808/sim808.cpp 

OBJS += \
./submodule/DFRobot_SIM808/DFRobot_sim808.o \
./submodule/DFRobot_SIM808/sim808.o 

CPP_DEPS += \
./submodule/DFRobot_SIM808/DFRobot_sim808.d \
./submodule/DFRobot_SIM808/sim808.d 


# Each subdirectory must supply rules for building sources it contributes
submodule/DFRobot_SIM808/%.o: ../submodule/DFRobot_SIM808/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU C++ Compiler'
	arm-none-eabi-g++ -mcpu=cortex-m4 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DUSE_FULL_ASSERT -DOS_USE_SEMIHOSTING -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -DSTM32F302xC -DSTM32F30X -DPIDIVER1_2 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f3-stdperiph" -I"/home/thomas/git/usbdiver_stm32/STM32_USB-FS-Device_Driver/inc" -I"/home/thomas/git/usbdiver_stm32/vcp/inc" -I"/home/thomas/git/usbdiver_stm32/submodule/stm32f3-serial" -I"/home/thomas/git/usbdiver_stm32/submodule/DFRobot_SIM808" -I"/home/thomas/git/usbdiver_stm32/submodule/iota-c-light-wallet/src/iota" -std=gnu++11 -fabi-version=0 -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


