################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../submodule/micro-light-wallet/src/keccak/sha3.c 

OBJS += \
./submodule/micro-light-wallet/src/keccak/sha3.o 

C_DEPS += \
./submodule/micro-light-wallet/src/keccak/sha3.d 


# Each subdirectory must supply rules for building sources it contributes
submodule/micro-light-wallet/src/keccak/%.o: ../submodule/micro-light-wallet/src/keccak/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DUSE_FULL_ASSERT -DOS_USE_SEMIHOSTING -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -DSTM32F302xC -DSTM32F30X -DPIDIVER1_2 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f3-stdperiph" -I"/home/thomas/git/usbdiver_stm32/STM32_USB-FS-Device_Driver/inc" -I"/home/thomas/git/usbdiver_stm32/vcp/inc" -I"/home/thomas/git/usbdiver_stm32/keccak" -I"/home/thomas/git/usbdiver_stm32" -I"/home/thomas/git/usbdiver_stm32/submodule/micro-light-wallet/src" -I"/home/thomas/git/usbdiver_stm32/submodule/micro-light-wallet/src/iota" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


