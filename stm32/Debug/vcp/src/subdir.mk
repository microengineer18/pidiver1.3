################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../vcp/src/hw_config.c \
../vcp/src/stm32_it.c \
../vcp/src/usb_desc.c \
../vcp/src/usb_endp.c \
../vcp/src/usb_istr.c \
../vcp/src/usb_prop.c \
../vcp/src/usb_pwr.c 

OBJS += \
./vcp/src/hw_config.o \
./vcp/src/stm32_it.o \
./vcp/src/usb_desc.o \
./vcp/src/usb_endp.o \
./vcp/src/usb_istr.o \
./vcp/src/usb_prop.o \
./vcp/src/usb_pwr.o 

C_DEPS += \
./vcp/src/hw_config.d \
./vcp/src/stm32_it.d \
./vcp/src/usb_desc.d \
./vcp/src/usb_endp.d \
./vcp/src/usb_istr.d \
./vcp/src/usb_prop.d \
./vcp/src/usb_pwr.d 


# Each subdirectory must supply rules for building sources it contributes
vcp/src/%.o: ../vcp/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DUSE_FULL_ASSERT -DOS_USE_SEMIHOSTING -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -DSTM32F302xC -DSTM32F30X -DPIDIVER1_2 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f3-stdperiph" -I"/home/thomas/git/usbdiver_stm32/STM32_USB-FS-Device_Driver/inc" -I"/home/thomas/git/usbdiver_stm32/vcp/inc" -I"/home/thomas/git/usbdiver_stm32/submodule/stm32f3-serial" -I"/home/thomas/git/usbdiver_stm32/submodule/DFRobot_SIM808" -I"/home/thomas/git/usbdiver_stm32/submodule/iota-c-light-wallet/src/iota" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


