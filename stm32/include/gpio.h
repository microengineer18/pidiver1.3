
#ifndef __gpio_H
#define __gpio_H


#include "main.h"


class GPIOPin {
private:
	GPIO_TypeDef *m_gpio;
	uint16_t m_pinnr;
	BitAction m_init;
	GPIOMode_TypeDef m_type;
	GPIOOType_TypeDef m_otype;
	GPIOPuPd_TypeDef m_pup;
public:
	GPIOPin(GPIO_TypeDef* gpio, uint16_t pinnr, GPIOMode_TypeDef type, GPIOOType_TypeDef otype, GPIOPuPd_TypeDef pup, BitAction init = Bit_RESET) {
		m_gpio = gpio;
		m_pinnr = pinnr;
		m_init = init;
		m_type = type;
		m_otype = otype;
		m_pup = pup;
	}
	GPIO_TypeDef* getGPIO() {
		return m_gpio;
	}
	uint16_t getPinNr() {
		return m_pinnr;
	}
	BitAction getInitValue() {
		return m_init;
	}
	GPIOMode_TypeDef getType() {
		return m_type;
	}
	GPIOOType_TypeDef getOType() {
		return m_otype;
	}
	GPIOPuPd_TypeDef getPUP() {
		return m_pup;
	}

};

class GPIOClass {
public:
	enum PinsEnum {
		DISC, LED, MOSI, MISO, SS, SCK, PIDETECT, CONFDONE, nSTATUS, DCK, DATA0, nCONFIG, NUM_ELEMENTS
	};

	GPIOPin* Pins[PinsEnum::NUM_ELEMENTS];

	GPIOClass();
	void init(bool fpgaconf);
	void set(GPIOPin* g);
	void clr(GPIOPin* g);
	void set(PinsEnum e);
	void clr(PinsEnum e);

	BitAction read(GPIOPin* g);
	BitAction read(PinsEnum e);
	GPIOPin* getGPIOPin(PinsEnum e);
};




//void init_gpio(void);


#endif

