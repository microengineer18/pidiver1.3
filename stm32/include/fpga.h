/*
 * fpga.h
 *
 *  Created on: 18.05.2018
 *      Author: ne23kaj2
 */

#ifndef FPGA_H_
#define FPGA_H_

#include "gpio.h"

//#define STATE_LENGTH 729
//#define HASH_LENGTH 243
#define FLAG_RUNNING                    (1<<0)
#define FLAG_FOUND                      (1<<1)
#define FLAG_OVERFLOW                   (1<<2)
#define FLAG_CURL_FINISHED              (1<<3)


#define FLAG_START                      (1<<0)
#define FLAG_CURL_RESET                 (1<<1)
#define FLAG_CURL_WRITE                 (1<<2)
#define FLAG_CURL_DO_CURL               (1<<3)

#define CMD_NOP                         0x00000000
#define CMD_WRITE_FLAGS                 0x04000000
#define CMD_RESET_WRPTR                 0x08000000
#define CMD_WRITE_DATA                  0x10000000
#define CMD_WRITE_MIN_WEIGHT_MAGNITUDE  0x20000000
#define CMD_READ_FLAGS                  0x84000000
#define CMD_READ_NONCE                  0x88000000
#define CMD_READ_CRC32                  0x90000000

#define FLAG_RESERVATION_RESET       (1 << 25)
#define FLAG_RESERVATION_WRITE       ((1 << 24) | (1 << 23))
#define FLAG_RESERVATION_WRITE_SHIFT 23
//efine FLAG_RESERVATION_PI                 = 0x1
#define FLAG_RESERVATION_USB         0x2

#define FLAG_RESERVATION_READ        ((1 << 23) | (1 << 22))
#define FLAG_RESERVATION_READ_SHIFT  22

class FPGAClass {
private:
	uint8_t	dataBuffer[512];
	GPIOClass* m_gpio;
protected:


public:
	FPGAClass(GPIOClass* gpio);
	bool uploadFPGA(uint8_t* dataBuffer, uint32_t dlen);
	void startConfigure();
	bool isConfigured();

	void lowLevelInit(void);
	void initSPI();


	void send(uint32_t cmd);
	uint32_t sendReceive(uint32_t cmd);
	void writeFlags(char flag_start);
	void resetWritePointer();
	void writeData(uint32_t tritshi, uint32_t tritslo);
	uint32_t readParallelLevel();
	uint32_t readBinaryNonce();
	void writeMinWeightMagnitude(int bits);
	uint32_t getMask();
	uint32_t getFlags();
	void curlBlock(uint32_t* data, uint8_t docurl);
	void curlInit();
	uint32_t reverse(uint32_t cmd);


	uint8_t sendByte(uint8_t byte);
	uint8_t readByte(void);

	void resetReservation();
	bool waitForReservation(uint32_t timeout);

};



#endif /* FPGA_H_ */
