//
// This file is part of the GNU ARM Eclipse distribution.
// Copyright (c) 2014 Liviu Ionescu.
//

// ----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "diag/Trace.h"

#include "gpio.h"

#include "Timer.h"
#include "hw_config.h"
#include "usb_lib.h"
#include "usb_desc.h"
#include "usb_pwr.h"
#include "spi_flash.h"
#include "fpga.h"
#include "hash.h"
#include "converter.h"

#define min(a,b) ((a<b)?a:b)

// Sample pragmas to cope with warnings. Please note the related line at
// the end of this function, used to pop the compiler diagnostics status.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wreturn-type"

#define STATE_ID			0x00
#define STATE_COMMAND		0x01
#define STATE_CRC8			0x02
#define STATE_LENGTH_LOW	0x03
#define STATE_LENGTH_HIGH	0x04
#define STATE_DATA			0x05
#define STATE_EXECUTE		0x06
#define STATE_ERROR			0xff

#define CMD_NOP				0x00
#define CMD_GET_VERSION		0x01
#define CMD_FLASH_ERASE		0x10
#define CMD_SET_PAGE		0x11
#define CMD_WRITE_PAGE		0x12
#define CMD_READ_PAGE		0x13
#define CMD_CONFIGURE_FPGA	0x14
#define CMD_READ_STATUS		0x15
#define CMD_CONFIGURE_FPGA_BLOCK	0x16
#define CMD_CONFIGURE_FPGA_START	0x17
#define CMD_RESERVATION			0x18
#define CMD_RESETVATION_RESET	0x19

#define MAX_MIN_WEIGHT_MAGNITUDE	26

#define CMD_POW				0x20

#define FLASH_SIZE			(1024 * 1024)	// 8MBit SPI Flash
#define FLASH_META_ADDR		(FLASH_SIZE - sFLASH_SPI_PAGESIZE)

struct Meta {
	uint64_t timestamp;
	char filename[32];
	uint32_t filesize;
	uint8_t autoconf;
}__attribute__ ((packed));

struct Status {
	uint8_t isFPGAConfigured;
}__attribute__ ((packed));

struct PoWResult {
	uint32_t nonce;
	uint32_t mask;
	uint32_t parallel;
	uint32_t time;
} __attribute__ ((packed));

struct Trytes {
	uint32_t data[33][27];
	uint32_t crc32[33];
	uint32_t mwm;
} __attribute__ ((packed));

struct Version {
	uint32_t major;
	uint32_t minor;
} __attribute__ ((packed)) version;

struct Reservation {
	uint32_t timeout;
	uint32_t status;
};

GPIOClass gpio;
FPGAClass fpga(&gpio);

uint32_t packet_sent = 1;
uint32_t packet_receive = 1;

#define RXBUFSIZE 1024
uint8_t rxbuf[RXBUFSIZE];
uint32_t rxbufrdptr = 0;
uint32_t rxbufwrptr = 0;

extern volatile uint32_t systick;

struct Com {
	uint8_t id;
	uint8_t cmd;
	uint8_t crc8;
	uint16_t length;
	uint8_t data[8192];
}__attribute__ ((packed)) com;

extern "C" void USB_CDC_ReceiveCallback(uint8_t* Receive_Buffer, uint32_t Receive_length) {
	for (uint32_t i = 0; i < Receive_length; i++) {
		rxbuf[rxbufwrptr % RXBUFSIZE] = Receive_Buffer[i];
		rxbufwrptr++;
	}
}

extern "C" uint32_t getSysTick() {
	return systick;
}

extern "C" void delay(uint32_t d) {
	uint32_t start = systick;
	while (systick - start < d + 1)
		;
}

uint8_t crc8_bytecalc(uint8_t reg, uint8_t byte) {
	uint8_t flag;			// flag um das oberste Bit zu merken
	uint8_t polynom = 0xd5;	// Generatorpolynom

	// gehe fuer jedes Bit der Nachricht durch
	for (int i = 0; i < 8; i++) {
		if (reg & 0x80)
			flag = 1;
		else
			flag = 0;	// Teste MSB des Registers
		reg <<= 1;							// Schiebe Register 1 Bit nach Links und
		if (byte & 0x80)
			reg |= 1;				// FÃžlle das LSB mit dem naechsten Bit der Nachricht auf
		byte <<= 1;							// nÃĪchstes Bit der Nachricht
		if (flag)
			reg ^= polynom;			// falls flag==1, dann XOR mit Polynom
	}
	return reg;
}

uint8_t crc8_messagecalc(uint8_t *msg, int len) {
	uint8_t reg = 0;
	for (int i = 0; i < len; i++) {
		reg = crc8_bytecalc(reg, msg[i]);			// Berechne fuer jeweils 8 Bit der Nachricht
	}
	return crc8_bytecalc(reg, 0);	// die Berechnung muss um die Bitlaenge des Polynoms mit 0-Wert fortgefuehrt werden
}

extern "C" void USB_SendData(Com* com) {
	com->crc8 = crc8_messagecalc(&com->data[0], com->length);
	int toSend = ((int) &com->data[0] - (int) &com->id) + com->length;
	int maxsize = VIRTUAL_COM_PORT_DATA_SIZE - 1;
	while (toSend) {
		int chunklen = min(maxsize, toSend);
		while (GetEPTxStatus(ENDP1) != EP_TX_NAK)
			;
		CDC_Send_DATA(&com->data[com->length - toSend], chunklen);
		toSend -= chunklen;
	}
}
static uint32_t selectedPage = 0;
static int configPageCtr = 0;

bool executeCommand(Com* com) {
	switch (com->cmd) {
	case CMD_NOP:	// loopback request to response
		break;
	case CMD_GET_VERSION:
		memcpy(&com->data[0], (uint8_t*) &version, sizeof(Version));
		com->length = sizeof(Version);
	case CMD_FLASH_ERASE:
		sFLASH_ClearWriteProtection();
		sFLASH_EraseBulk();
		break;
	case CMD_SET_PAGE:
		selectedPage = *((uint32_t*) &com->data[0]);
		break;
	case CMD_WRITE_PAGE:
		if (com->length != sFLASH_SPI_PAGESIZE)
			return false;
		for (int i = 0; i < sFLASH_SPI_PAGESIZE; i++) {
			sFLASH_WriteSingleByte(com->data[i], selectedPage * sFLASH_SPI_PAGESIZE + i);
		}
//		sFLASH_WriteBuffer(&com->data[0], selectedPage * sFLASH_SPI_PAGESIZE, sFLASH_SPI_PAGESIZE);
		break;
	case CMD_READ_PAGE:
		sFLASH_ReadBuffer(&com->data[0], selectedPage * sFLASH_SPI_PAGESIZE, sFLASH_SPI_PAGESIZE);
		com->length = sFLASH_SPI_PAGESIZE;
		break;
	case CMD_CONFIGURE_FPGA: {
		Meta meta;
		// read meta information
		sFLASH_ReadBuffer((uint8_t*) &meta, FLASH_META_ADDR, sizeof(Meta));
		// was config file flashed?
		if (meta.filesize > FLASH_SIZE) {
			return false;
		}
		fpga.startConfigure();
		uint32_t toFlash = meta.filesize;
		uint32_t offset = 0;
		uint8_t counter = 0;
		while (toFlash) {
			if (counter & 0x04)
				gpio.set(GPIOClass::LED);
			else
				gpio.clr(GPIOClass::LED);
			int chunk = min(toFlash, sFLASH_SPI_PAGESIZE);
			sFLASH_ReadBuffer(&com->data[0], offset, sFLASH_SPI_PAGESIZE);
			fpga.uploadFPGA(&com->data[0], chunk);
			offset += chunk;
			toFlash -= chunk;
			counter++;
		}
		com->length = 1;
		com->data[0] = (fpga.isConfigured()) ? 0x01 : 0x00;

		if (com->data[0])
			gpio.set(GPIOClass::LED);
		else
			gpio.clr(GPIOClass::LED);
		break;
	}
	case CMD_RESETVATION_RESET:
		fpga.resetReservation();
		break;
	case CMD_RESERVATION: {
		Reservation* res = (Reservation*) &com->data[0];
		if (res->timeout > 10000) {
			res->timeout = 10000;
		}
		res->status = (fpga.waitForReservation(res->timeout)) ? 1 : 0;
		com->length = sizeof(Reservation);
		break;
	}
	case CMD_CONFIGURE_FPGA_BLOCK: {
		configPageCtr++;
		if (configPageCtr & 0x04)
			gpio.set(GPIOClass::LED);
		else
			gpio.clr(GPIOClass::LED);
		fpga.uploadFPGA(&com->data[0], com->length);
		com->length = 1;
		com->data[0] = (fpga.isConfigured()) ? 0x01 : 0x00;

		if (com->data[0])
			gpio.set(GPIOClass::LED);
		else
			gpio.clr(GPIOClass::LED);
		break;
	}
	case CMD_CONFIGURE_FPGA_START:
		fpga.startConfigure();
		configPageCtr=0;
		break;
	case CMD_READ_STATUS: {
		Status* status = (Status*) &com->data[0];
		status->isFPGAConfigured = (fpga.isConfigured()) ? 0x01 : 0x00;
		com->length = sizeof(Status);
		break;
	}
	case CMD_POW: {
		if (com->length != sizeof(Trytes))
			return false;

		uint32_t startTicks = systick;
		Trytes* trytes = (Trytes*) &com->data[0];

		if (trytes->mwm == 0 || trytes->mwm > MAX_MIN_WEIGHT_MAGNITUDE)
			return false;

		// try to get reservation ... after 5sek unlock and try again
		if (!fpga.waitForReservation(5000)) {
			fpga.resetReservation();
			if (!fpga.waitForReservation(5000)) {
				return false;
			}
		}

		fpga.curlInit();
		for (int i = 0; i < 33; i++) { 	// 33 blocks to curl
			fpga.curlBlock(&trytes->data[i][0], (i != 32) ? 1 : 0);
		}
		fpga.writeMinWeightMagnitude(trytes->mwm);
		fpga.writeFlags(1);

		while (1) {
			uint32_t flags = fpga.getFlags();

			if (!(flags & FLAG_RUNNING) && ((flags & FLAG_FOUND) || flags & FLAG_OVERFLOW)) {
				break;
			}
			asm("nop");
		}
		PoWResult* pow = (PoWResult*) &com->data[0];
		pow->nonce = fpga.readBinaryNonce()-2; // -2 because of pipelining for speed on FPGA
		pow->parallel = fpga.readParallelLevel();
		pow->mask = fpga.getMask() & ((1<<pow->parallel)-1);
		pow->time = systick - startTicks;
		com->length = sizeof(PoWResult);

		fpga.resetReservation();
	}
	}
	return true;
}

int main(int argc, char* argv[]) {
	version.major = 1;
	version.minor = 1;

	Timer timer;
	timer.start();

	gpio.init(false);	// skip fpga conf-pins for now

	sFLASH_Init();
	uint8_t metaBuffer[sFLASH_SPI_PAGESIZE]={0};
	sFLASH_ReadBuffer(&metaBuffer[0], FLASH_META_ADDR, sFLASH_SPI_PAGESIZE);
	Meta* meta = (Meta*) &metaBuffer[0];
	if (meta->timestamp != 0xffffffffffffffff && meta->autoconf == 0x01) {
		// initialize config pins
		gpio.init(true);

		Com com;
		com.cmd = CMD_CONFIGURE_FPGA;
		executeCommand(&com);

		// deinit gpio and set to pullup configuration?
	}

	// if pi not detected configure config pins
	if (gpio.read(GPIOClass::PIDETECT)) {
		gpio.init(true);
	}

	fpga.initSPI();
	uint32_t id = sFLASH_ReadID();
	trace_printf("id: %08x\n", id);

	uint32_t parallel = fpga.readParallelLevel();
	trace_printf("parallel: %08x\n", parallel);

	trace_printf("flags %08x\n", fpga.getFlags());
	Set_System();
	Set_USBClock();
	USB_Interrupts_Config();
	USB_Init();

	/*	sFLASH_ClearWriteProtection();
	 sFLASH_EraseBulk();
	 uint8_t buf[256]={0x00};
	 sFLASH_ReadBuffer(&buf[0], 0, sFLASH_SPI_PAGESIZE);

	 for (int i=0;i<256;i++) {
	 trace_printf("%02x ", buf[i]);
	 }
	 trace_printf("\n");
	 */

	SystemCoreClockUpdate();

	// enable USB pullup for enumeration
	gpio.set(GPIOClass::DISC);
#ifdef DEBUG
	trace_printf("System clock: %u Hz\n", SystemCoreClock);
#endif

	uint32_t state = STATE_ID;
	uint32_t timeout = 0;
	int count = 0;
	while (1) {
		// reset state if no data for > 1000ms
		if (timeout && (systick - timeout) > 1000) {
			state = STATE_ID;
		}
		if (bDeviceState == CONFIGURED) {
			CDC_Receive_DATA();
			// enough space in rx buf?
			//__disable_irq();
			for (; rxbufrdptr < rxbufwrptr; rxbufrdptr++) {
				uint8_t data = rxbuf[rxbufrdptr % RXBUFSIZE];
				switch (state) {
				case STATE_ID:
					com.id = data;
					state = STATE_COMMAND;
					timeout = systick;
					break;
				case STATE_COMMAND:
					com.cmd = data;
					com.length = 0;
					com.length = 0;
					com.crc8 = 0;
					count = 0;
					state = STATE_CRC8;
					break;
				case STATE_CRC8:
					com.crc8 = data;
					state = STATE_LENGTH_LOW;
					break;
				case STATE_LENGTH_LOW:
					com.length = data;
					state = STATE_LENGTH_HIGH;
					break;
				case STATE_LENGTH_HIGH:
					com.length |= (uint16_t) data << 8;
					if (com.length > sizeof(com.data)) {
						state = STATE_ERROR;
						break;
					}
					state = STATE_DATA;
					break;
				case STATE_DATA:
					com.data[count] = data;
					count++;
					if (count == com.length) {
						if (crc8_messagecalc(&com.data[0], com.length) != com.crc8) {
							state = STATE_ERROR;
							break;
						}

						if (!executeCommand(&com)) {
							state = STATE_ERROR;
							break;
						}

						// send back struct
						USB_SendData(&com);

						state = STATE_ID;
						timeout = 0;
					}
					break;
				}
				if (state == STATE_ERROR) {
					uint8_t X = 'X';
					CDC_Send_DATA(&X, 1);
					state = STATE_ID;
					timeout = 0;
				}
			}

		}
	}

	return 0;
}

#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------
