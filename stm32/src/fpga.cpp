/*
 * fpga.cpp
 *
 *  Created on: 18.05.2018
 *      Author: ne23kaj2
 */


#include "gpio.h"
#include "fpga.h"
#include "Timer.h"

#define SPIx	SPI1

extern Timer timer;
extern volatile uint32_t systick;

FPGAClass::FPGAClass(GPIOClass* gpio) {
	this->m_gpio = gpio;
}

bool FPGAClass::uploadFPGA(uint8_t* dataBuffer, uint32_t dlen) {
	uint8_t* fpga_data = &dataBuffer[0];
	uint8_t* fpga_flash_data_end = fpga_data + dlen;

	do {
		uint8_t value = *fpga_data++;
		for (int i = 0; i < 8; i++, value >>= 1) {
			if (value & 1) {
				/* bit set -> toggle DATA0 to high */
				m_gpio->set(GPIOClass::DATA0);
			} else {
				/* bit is cleared -> toggle DATA0 to low */
				m_gpio->clr(GPIOClass::DATA0);
			}
			/* toggle DCLK -> FPGA reads the bit */
			m_gpio->set(GPIOClass::DCK);
			m_gpio->clr(GPIOClass::DCK);
		}
	} while ((!(m_gpio->read(GPIOClass::CONFDONE))) && (fpga_data < fpga_flash_data_end));

	return true;
}

void FPGAClass::startConfigure() {
	m_gpio->clr(GPIOClass::DCK);
	m_gpio->clr(GPIOClass::nCONFIG);

	timer.sleep(10);

	while (m_gpio->read(GPIOClass::nSTATUS) && m_gpio->read(GPIOClass::CONFDONE))
		;

	m_gpio->set(GPIOClass::nCONFIG);
	while (!m_gpio->read(GPIOClass::nSTATUS))
		;
}

bool FPGAClass::isConfigured() {
	return m_gpio->read(GPIOClass::CONFDONE) ? true : false;
}

void FPGAClass::initSPI() {
	SPI_InitTypeDef SPI_InitStructure;

	lowLevelInit();
	SPI_I2S_DeInit(SPIx);

	SPI_StructInit(&SPI_InitStructure);

	/*!< Deselect the FLASH: Chip Select high */
	m_gpio->set(GPIOClass::SS);

	/*!< SPI configuration */
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;

	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CRCPolynomial = 7;
	SPI_Init(SPIx, &SPI_InitStructure);
	SPI_RxFIFOThresholdConfig(SPIx, SPI_RxFIFOThreshold_QF);  // SPI1->CR2 |= SPI_CR2_FRXTH;

	/*!< Enable the sFLASH_SPI  */
	SPI_Cmd(SPIx, ENABLE);
}

void FPGAClass::lowLevelInit(void) {
	GPIO_InitTypeDef GPIO_InitStructure;

	/*!< Enable the SPI clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);

	/*!< Enable GPIO clocks */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

	/*!< SPI pins configuration *************************************************/

	/*!< Connect SPI pins to AF5 */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource5, GPIO_AF_5);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource6, GPIO_AF_5);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_5);

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;

	/*!< SPI SCK pin configuration */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/*!< SPI MOSI pin configuration */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/*!< SPI MISO pin configuration */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/*!< Configure sFLASH Card CS pin in output pushpull mode ********************/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
}

uint32_t FPGAClass::reverse(uint32_t cmd) {
	uint32_t rev = 0x00000000;

	rev |= (cmd & 0xff000000) >> 24;
	rev |= (cmd & 0x00ff0000) >> 8;
	rev |= (cmd & 0x0000ff00) << 8;
	rev |= (cmd & 0x000000ff) << 24;

	return rev;
}

uint8_t FPGAClass::sendByte(uint8_t byte) {
	while (SPI_I2S_GetFlagStatus(SPIx, SPI_I2S_FLAG_TXE) == RESET)
		;
	SPI_SendData8(SPIx, byte);
	while (SPI_I2S_GetFlagStatus(SPIx, SPI_I2S_FLAG_RXNE) == RESET)
		;
	return SPI_ReceiveData8(SPIx);
}

uint8_t FPGAClass::readByte(void) {
	return (sendByte(0x00));
}

void FPGAClass::send(uint32_t cmd) {
	uint32_t rev = reverse(cmd);
	m_gpio->clr(GPIOClass::SS);
	uint8_t* p = (uint8_t*) &rev;
	for (int i = 0; i < 4; i++) {
		sendByte(p[i]);
	}
//    bcm2835_spi_transfern((char*) &rev, 4);
	m_gpio->set(GPIOClass::SS);
	asm("nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;");

}

uint32_t FPGAClass::sendReceive(uint32_t cmd) {
	uint32_t rev = reverse(cmd);
	uint32_t rcv = 0x00000000;

	m_gpio->clr(GPIOClass::SS);
//    bcm2835_spi_transfern((char*) &rev, 4);
	uint8_t* p = (uint8_t*) &rev;
	for (int i = 0; i < 4; i++) {
		sendByte(p[i]);
	}
	m_gpio->set(GPIOClass::SS);
	asm("nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;");
	m_gpio->clr(GPIOClass::SS);
	uint8_t* d = (uint8_t*) &rcv;
	for (int i = 0; i < 4; i++) {
		d[i] = readByte();
	}
//    bcm2835_spi_transfernb((char*) &zero, (char*) &rcv, 4);
	m_gpio->set(GPIOClass::SS);
	asm("nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;");


	rcv = reverse(rcv);
//    printf("sent: %08x received: %08x\n",cmd, rcv);
	return rcv;
}

// write flags
void FPGAClass::writeFlags(char flag_start) {
	uint32_t cmd = CMD_WRITE_FLAGS;

	if (flag_start)
		cmd |= 0x00000001;

	send(cmd);
}

void FPGAClass::resetWritePointer() {
	uint32_t cmd = CMD_RESET_WRPTR;
	send(cmd);
}

void FPGAClass::writeData(uint32_t tritshi, uint32_t tritslo) {
	uint32_t cmd = CMD_WRITE_DATA;

	cmd |= tritslo & 0x000001ff;
	cmd |= (tritshi & 0x000001ff) << 9;

	send(cmd);
	/*
	 uint32_t sent = sendReceive(cmd);
	 if (sent != cmd) {
	 printf("verify error! %08x vs %08x\n", cmd, sent);
	 }*/
}

uint32_t FPGAClass::readParallelLevel() {
	uint32_t cmd = CMD_READ_FLAGS;
	return (sendReceive(cmd) & 0x000000f0) >> 4;
}

uint32_t FPGAClass::readBinaryNonce() {
	uint32_t cmd = CMD_READ_NONCE;
	return sendReceive(cmd);
}

void FPGAClass::writeMinWeightMagnitude(int bits) {
	uint32_t cmd = CMD_WRITE_MIN_WEIGHT_MAGNITUDE;

	if (bits > 26)
		bits = 26;

	// generate bitmask
	cmd |= (1 << bits) - 1;

	send(cmd);
}

uint32_t FPGAClass::getMask() {
	uint32_t cmd = CMD_READ_FLAGS;
	return ((sendReceive(cmd) >> 8) & 0x000000ff);  //((1<<m_parallel)-1));
}

uint32_t FPGAClass::getFlags() {
	uint32_t cmd = CMD_READ_FLAGS;
	return sendReceive(cmd) & 0x0000000f;
}

// write curl mid-state on FPGA and perform curl optionally
void FPGAClass::curlBlock(uint32_t* data, uint8_t docurl) {
	resetWritePointer();

	// data was prepared (incl command) on USB-host-side
	for (uint32_t i = 0; i < 27; i++) {
		send(data[i]);
	}

	uint32_t cmd = CMD_WRITE_FLAGS | FLAG_CURL_WRITE | ((docurl) ? FLAG_CURL_DO_CURL : 0);
	send(cmd);

	while (!(getFlags() & FLAG_CURL_FINISHED)) {
		asm("nop");
	}
}

// initialize mid-state on FPGA
void FPGAClass::curlInit() {
	uint32_t cmd = CMD_WRITE_FLAGS | FLAG_CURL_RESET;
	send(cmd);

	while (!(getFlags() & FLAG_CURL_FINISHED)) {
		asm("nop");
	}
}


void FPGAClass::resetReservation() {
	uint32_t cmd = CMD_WRITE_FLAGS | FLAG_RESERVATION_RESET;
	send(cmd);
}

bool FPGAClass::waitForReservation(uint32_t timeout) {
	uint32_t start = systick;
	while (1) {
		// make reservation
		send(CMD_WRITE_FLAGS | (FLAG_RESERVATION_USB << FLAG_RESERVATION_WRITE_SHIFT));
		uint32_t flags = sendReceive(CMD_READ_FLAGS);
		// if yes, then return
		if ((flags & FLAG_RESERVATION_READ) == (FLAG_RESERVATION_USB << FLAG_RESERVATION_READ_SHIFT)) {
			return true;
		}
		// no wait and try again
		if (systick - start  > timeout) {
			return false;
		}
		timer.sleep(50);
	}

}


