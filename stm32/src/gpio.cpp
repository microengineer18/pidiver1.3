#include "gpio.h"
#include "stm32f30x_gpio.h"


#define GPIOA_LED		GPIO_Pin_0

#define SPIx_RCC      RCC_APB1Periph_SPI3
#define SPIx          SPI3
#define SPI_GPIO_RCC  RCC_APB2Periph_GPIOB
//#define SPI_GPIO      GPIOB
#define GPIOA_MOSI  GPIO_Pin_7
#define GPIOA_MISO  GPIO_Pin_6
#define GPIOA_SCK  	GPIO_Pin_5
#define GPIOA_SS   	GPIO_Pin_3

#define GPIOB_nSTATUS	GPIO_Pin_4
#define GPIOB_DCK		GPIO_Pin_7
#define GPIOB_DATA0		GPIO_Pin_8
#define GPIOB_nCONFIG	GPIO_Pin_9
#define GPIOA_CONFDONE	GPIO_Pin_2
#define GPIOA_PIDETECT	GPIO_Pin_15

#define GPIOB_DISC		GPIO_Pin_2

void GPIOClass::set(GPIOPin* g) {
	GPIO_WriteBit(g->getGPIO(), g->getPinNr(), Bit_SET);
}

void GPIOClass::clr(GPIOPin* g) {
	GPIO_WriteBit(g->getGPIO(), g->getPinNr(), Bit_RESET);
}

void GPIOClass::set(PinsEnum e) {
	set(getGPIOPin(e));
}

void GPIOClass::clr(PinsEnum e) {
	clr(getGPIOPin(e));
}

GPIOPin* GPIOClass::getGPIOPin(PinsEnum e) {
	return Pins[e];
}

BitAction GPIOClass::read(GPIOPin* g) {
	return GPIO_ReadInputDataBit(g->getGPIO(), g->getPinNr()) ? Bit_SET : Bit_RESET;
}

BitAction GPIOClass::read(PinsEnum e) {
	return read(getGPIOPin(e));
}


GPIOClass::GPIOClass() {
Pins[PinsEnum::DISC] = new GPIOPin(GPIOB, 		GPIOB_DISC, 		GPIO_Mode_OUT,  GPIO_OType_PP, GPIO_PuPd_NOPULL, Bit_RESET);
	Pins[PinsEnum::LED] = new GPIOPin(GPIOA, 		GPIOA_LED, 			GPIO_Mode_OUT,  GPIO_OType_PP, GPIO_PuPd_NOPULL, Bit_SET);
	Pins[PinsEnum::MOSI] = new GPIOPin(GPIOA, 		GPIOA_MOSI, 		GPIO_Mode_AF,  GPIO_OType_PP, GPIO_PuPd_NOPULL, Bit_SET);
	Pins[PinsEnum::MISO] = new GPIOPin(GPIOA, 		GPIOA_MISO, 		GPIO_Mode_AF,  GPIO_OType_PP, GPIO_PuPd_NOPULL, Bit_SET);
	Pins[PinsEnum::SS] = new GPIOPin(GPIOA, 		GPIOA_SS, 			GPIO_Mode_OUT,  GPIO_OType_PP, GPIO_PuPd_NOPULL, Bit_SET);
	Pins[PinsEnum::SCK] = new GPIOPin(GPIOA, 		GPIOA_SCK, 			GPIO_Mode_AF,  GPIO_OType_PP, GPIO_PuPd_NOPULL, Bit_SET);
	Pins[PinsEnum::PIDETECT] = new GPIOPin(GPIOA, 	GPIOA_PIDETECT, 	GPIO_Mode_IN,  GPIO_OType_PP, GPIO_PuPd_NOPULL, Bit_SET);
	Pins[PinsEnum::CONFDONE] = new GPIOPin(GPIOA, 	GPIOA_CONFDONE, 	GPIO_Mode_IN,  GPIO_OType_PP, GPIO_PuPd_NOPULL, Bit_SET);
	Pins[PinsEnum::nSTATUS] = new GPIOPin(GPIOB, 	GPIOB_nSTATUS, 		GPIO_Mode_IN,  GPIO_OType_PP, GPIO_PuPd_NOPULL, Bit_SET);
	Pins[PinsEnum::DCK] = new GPIOPin(GPIOB, 		GPIOB_DCK, 			GPIO_Mode_OUT,  GPIO_OType_PP, GPIO_PuPd_NOPULL, Bit_SET);
	Pins[PinsEnum::DATA0] = new GPIOPin(GPIOB, 		GPIOB_DATA0, 		GPIO_Mode_OUT,  GPIO_OType_PP, GPIO_PuPd_NOPULL, Bit_SET);
	Pins[PinsEnum::nCONFIG] = new GPIOPin(GPIOB, 	GPIOB_nCONFIG, 		GPIO_Mode_OUT,  GPIO_OType_PP, GPIO_PuPd_NOPULL, Bit_SET);
}

void GPIOClass::init(bool fpgaconf) {
	GPIO_InitTypeDef GPIO_InitStruct;
RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);
	for (int i=0;i<(fpgaconf ? PinsEnum::NUM_ELEMENTS : PinsEnum::CONFDONE);i++) {
		if (Pins[i] && Pins[i]->getInitValue() == Bit_SET) {
			set(Pins[i]);
		} else
			clr(Pins[i]);
	}

	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	for (int i=0;i<(fpgaconf ? PinsEnum::NUM_ELEMENTS : PinsEnum::CONFDONE);i++) {
		if (!Pins[i])
			continue;
		GPIO_InitStruct.GPIO_Pin = Pins[i]->getPinNr();
		GPIO_InitStruct.GPIO_Mode = Pins[i]->getType();
		GPIO_InitStruct.GPIO_OType = Pins[i]->getOType();
		GPIO_InitStruct.GPIO_PuPd = Pins[i]->getPUP();
		GPIO_Init(Pins[i]->getGPIO(), &GPIO_InitStruct);
	}

}
